import unittest

import pandapower as pp

from midas_powergrid.custom.bhv import build_grid
from midas_powergrid.model.static import PandapowerGrid


class TestBremerhaven(unittest.TestCase):
    def test_powerflow(self):
        grid = build_grid()
        self.assertTrue(grid.res_bus.empty)
        try:
            pp.runpp(grid)
        except pp.LoadflowNotConverged:
            self.assertTrue(False, "Load flow failed")
        self.assertFalse(grid.res_bus.empty)

    def test_powerflow_with_extension(self):
        grid = build_grid(use_extension=True)
        self.assertTrue(grid.res_bus.empty)
        try:
            pp.runpp(grid)
        except pp.LoadflowNotConverged:
            self.assertTrue(False, "Load flow failed")
        self.assertFalse(grid.res_bus.empty)

    def test_with_constraints(self):
        gparams = {
            "gridfile": "bhv",
            "constraints": [
                ["line", 100],
                ["load", 0.02],
                ["sgen", 0.05],
                ["bus", 0.1],
            ],
            "disable_numba": False
        }

        grid = PandapowerGrid(gparams)

        try:
            grid.run_powerflow(0)
        except pp.LoadflowNotConverged:
            self.assertTrue(False, "Load flow failed")
        self.assertFalse(grid.grid._grid.res_bus.empty)


if __name__ == "__main__":
    unittest.main()
